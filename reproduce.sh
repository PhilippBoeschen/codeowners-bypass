#!/bin/sh

REPO_PATH=$1
APPROVER=$2

git clone git@gitlab.com:$REPO_PATH.git codeowners-bypass-reproduction
cd codeowners-bypass-reproduction

echo "change #1" > a.txt
git add a.txt 
git commit -m "Add a.txt" a.txt 
echo "/a.txt @$APPROVER" > CODEOWNERS
git add CODEOWNERS 
git commit -m "Add codeowners"
git mv a.txt b.txt
echo "/b.txt @$APPROVER" > CODEOWNERS
git commit -am "Moving a.txt to b.txt with codeowners"
git push -u origin main
# negative case where codeowners are bypassed
git checkout -b codeowners-bypass HEAD^
echo "change #2" >> a.txt
git commit -am "Add new line to a.txt"
git push -u origin codeowners-bypass 
echo "Click on the link in the remote above to create an MR"
# positive case where this works
git checkout main 
git checkout -b codeowners-approval-works HEAD
echo >> "Change #3" >> b.txt
git add b.txt 
git commit -m "Adding line to b.txt"
git push -u origin codeowners-approval-works
echo "Click on the link in the remote above to create an MR"
